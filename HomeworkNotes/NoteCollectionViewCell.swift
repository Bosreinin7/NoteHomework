//
//  NoteCollectionViewCell.swift
//  HomeworkNotes
//
//  Created by SreiNin Bo on 12/20/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class NoteCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
}
