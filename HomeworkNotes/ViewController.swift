//
//  ViewController.swift
//  HomeworkNotes
//
//  Created by SreiNin Bo on 12/18/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit
import  CoreData

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate {
    
    
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    @IBOutlet weak var microphoneButton: UIBarButtonItem!
    @IBOutlet weak var penButton: UIBarButtonItem!
    @IBOutlet weak var listButton: UIBarButtonItem!
    @IBOutlet weak var takeNote: UIBarButtonItem!
    @IBOutlet weak var myCollectionView: UICollectionView!
    var Title:String?
    var descript:String?
    var Context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var notes:[Notes]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNoteButton()
        //Custom Title "Notes"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        //Get Data for CoreData
        notes = try? Context.fetch(Notes.fetchRequest())
        //Collection View
        
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        layout.itemSize = CGSize(width: (view.frame.size.width - 20) / 2, height: view.frame.size.height/4)
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 4
        myCollectionView.collectionViewLayout = layout
        
        
        let holdToDelete : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gesture:)))
        holdToDelete.minimumPressDuration = 0.5
        holdToDelete.delegate = self
        holdToDelete.delaysTouchesBegan = true
        self.myCollectionView?.addGestureRecognizer(holdToDelete)

       
    }
    override func viewWillAppear(_ animated: Bool) {
        getData()
        myCollectionView.reloadData()

    }
    func getData() {
        self.notes = try? Context.fetch(Notes.fetchRequest())
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (notes?.count)!
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = myCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NoteCollectionViewCell
        Cell.titleLabel.text = notes?[indexPath.row].title
        Cell.descriptionLabel.text = notes?[indexPath.row].descrip
        
        
        return Cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = myCollectionView.frame.width/3 - 1
        return CGSize(width: size, height: size)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to second sotryboard
        let second = self.storyboard?.instantiateViewController(withIdentifier: "secondStoryboard") as! DescriptionAddViewController

        second.Title = notes?[indexPath.row].title
        second.descript = notes?[indexPath.row].descrip
        second.IndexNote = indexPath.row
        second.status = true
        self.navigationController?.pushViewController(second, animated: true)
    }
    func addNoteButton() {
        cameraButton.action = #selector(addNewNote)
        microphoneButton.action = #selector(addNewNote)
        penButton.action = #selector(addNewNote)
        listButton.action = #selector(addNewNote)
        takeNote.action = #selector(addNewNote)
    }

    @objc func addNewNote() {
        performSegue(withIdentifier:"showAdd", sender: nil)
    }
    
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        if gesture.state != .ended {
            return
        }
        
        let p = gesture.location(in: self.myCollectionView)
        
        if let indexPath = self.myCollectionView.indexPathForItem(at: p) {
            
            let alert = UIAlertController(title: "Are you sure to delete this note?", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {action in
                let note = self.notes?[indexPath.row]
                self.Context.delete(note!)
                self.appDelegate.saveContext()
                self.getData()
                self.myCollectionView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true)
        } else {
            print("couldn't find index path")
        }
    }

}



