//
//  DescriptionAddViewController.swift
//  HomeworkNotes
//
//  Created by SreiNin Bo on 12/20/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class DescriptionAddViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    var Note:[Notes]?
    var Title:String?
    var descript:String?
    var status: Bool = false
    var IndexNote: Int?
    var Context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.descriptionTextView.delegate = self
        if status {
            self.titleTextField.text = Title
            self.descriptionTextView.text = descript
        }
        
        if descriptionTextView.text != "Note" {
            descriptionTextView.textColor = UIColor.black
            
        }

    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = "Note"
            textView.textColor = UIColor.lightGray
        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "Note" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        return true
    }
    
    //=============Befor back to first Screen===============
    override func viewWillDisappear(_ animated: Bool) {
            if status {
                upDateData()
             }
            else {
                
                addData()
          
            }
        
    }
    
    func addData(){
        if titleTextField.text == "" && descriptionTextView.text == "Note"{
           
            return
        }
        if descriptionTextView.text == "Note"{
             descriptionTextView.text = ""
        }
        let note = Notes(context:Context)
        note.title = self.titleTextField.text
        note.descrip = self.descriptionTextView.text
        appDelegate.saveContext()
        
    }
    func upDateData(){
        
        Note = try? Context.fetch(Notes.fetchRequest())
        if titleTextField.text == "" && (descriptionTextView.text == "Note" || descriptionTextView.text == "" ){
            let note = self.Note?[IndexNote!]
            self.Context.delete(note!)
            appDelegate.saveContext()
            return
        }

        Note![IndexNote!].title = titleTextField.text
        Note![IndexNote!].descrip = descriptionTextView.text
        appDelegate.saveContext()

    }

    
    @IBAction func AddButtonNote(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take photo", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Choose image", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Drawing", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Recording", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Checkboxs", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true)
        
    }
    
    @IBAction func MoreButtonNote(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Make a copy", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Send", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Collabrators", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Labels", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true)

        
    }
}

